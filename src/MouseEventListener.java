import java.awt.event.*;

public class MouseEventListener implements MouseListener, MouseMotionListener, MouseWheelListener {
    private Game game;

    MouseEventListener(Game game){
        this.game = game;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1)game.leftClick(e.getX(), e.getY());
        if(e.getButton() == MouseEvent.BUTTON3)game.rightClick(e.getX(), e.getY());

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        //Make this control blocks to place in admin mode
    }
}
