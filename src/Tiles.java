import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Tiles {

    private SpriteSheet spriteSheet;
    private ArrayList<Tile> tileList = new ArrayList<Tile>();
    private static int tileListSize=0;

    public Tiles(File tilesFile, SpriteSheet spriteSheet){
        this.spriteSheet = spriteSheet;
        try {
            Scanner scanner = new Scanner(tilesFile);
            while(scanner.hasNextLine()){
                String line = scanner.nextLine();
                if(!line.startsWith("//")){
                    String[] splitString = line.split("-");
                    String tileName = splitString[0];
                    int spriteX = Integer.parseInt(splitString[1]);
                    int spriteY = Integer.parseInt(splitString[2]);
                    Tile tile = new Tile(tileName, spriteSheet.getSprite(spriteX,spriteY));

                    if(splitString.length >= 4) {
                        tile.collidable = true;
                        tile.collisionType = Integer.parseInt(splitString[3]);
                    }

                    tileList.add(tile);
                }
            }
            tileListSize = tileList.size()-1;
        }catch (FileNotFoundException e){
            System.out.println("Could not find the tiles config file");
            e.printStackTrace();
        }

    }//Assumes spriteSheet has been loaded

    public void renderTile(int tileID, RenderHandler renderer, int xPosition, int yPosition, int xZoom, int yZoom){
        if(tileList.size()>tileID && tileID>=0){
            renderer.renderSprite(tileList.get(tileID).sprite, xPosition, yPosition, xZoom, yZoom);

        }else{
            System.out.println("TileID out of bounds exception");
        }
    }

    public int collisionType(int tileID)
    {
        if(tileID >= 0 && tileList.size() > tileID)
        {
            return tileList.get(tileID).collisionType;
        }
        else
        {
            System.out.println("TileID " + tileID + " is not within range " + tileList.size() + ".");
        }
        return -1;
    }

    public static int getTileListLen(){return tileListSize;}
    public Tile getTileByID(int ID){
        return tileList.get(ID);
    }
}
