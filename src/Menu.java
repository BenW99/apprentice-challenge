import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu extends JFrame{

    Boolean sound = true;
    JPanel cards = new JPanel(new CardLayout());
    CardLayout cl = (CardLayout)(cards.getLayout());
    Options optionsPanel = new Options(cards, cl, sound);
    KeyBoardListener keyListener = new KeyBoardListener();
    KeyBinding keyBindingPanel = new KeyBinding(cards, cl, keyListener);
    JPanel menuPanel = new JPanel();
    Sound soundControls = new Sound();
    JPanel backPanel = new JPanel();

    public Menu()
    {
        setContentPane(new JLabel(new ImageIcon("./resources/startUp/doctor-dale-menu.png")));
        setLayout(new GridBagLayout());

        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        backPanel.setLayout(new GridLayout(1,3, 110,0));
        backPanel.add(createButton("START", this));
        backPanel.add(createButton("OPTIONS", this));
        backPanel.add(createButton("EXIT", this));

        menuPanel.setLayout(new GridBagLayout());
        menuPanel.add(backPanel, new GridBagConstraints());

        //Sets all the panels to transparent
        menuPanel.setOpaque(false);
        cards.setOpaque(false);
        backPanel.setOpaque(false);
        optionsPanel.setOpaque(false);
        keyBindingPanel.setOpaque(false);

        cards.add(menuPanel, "menu");
        cards.add(optionsPanel, "options");
        cards.add(keyBindingPanel, "keyBinding");
        add(cards);
        cl.show(cards, "menu");
    }

    private JButton createButton(String title, Menu menu)
    {
        JButton button = new JButton(title);
        button.setPreferredSize(new Dimension(100, 100));
        button.setBackground(Color.decode("#d3d3d3"));
        class buttonChange implements ActionListener
        {
            public void actionPerformed(ActionEvent event)
            {
                if (title.equals("START")){
                    Game game = new Game(menu, keyListener, soundControls);
                    Thread gameThread = new Thread(game);
                    gameThread.start();
                    dispose();
                }else if(title.equals("OPTIONS")){
                    cl.show(cards, "options");
                }else if(title.equals("EXIT")){
                    System.exit(0);
                }
            }
        }
        ActionListener listener = new buttonChange();
        button.addActionListener(listener);
        return button;
    }
}

