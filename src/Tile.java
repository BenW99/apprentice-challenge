public class Tile {
    private String type;
    public boolean collidable = false;
    public int collisionType = -1;
    Sprite sprite;

    public Tile(String type, Sprite sprite){
            this.type = type;
            this.sprite = sprite;
    }

}
