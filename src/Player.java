public class Player extends Character {



    Player(Sprite sprite, int xZoom, int yZoom){
        super(sprite, 5, 10, 10, xZoom, yZoom);
    }

    @Override
    public void update(Game game) {
        KeyBoardListener keyListener = game.getKeyListener();
        String dirString = "";
        if(keyListener.up())
            dirString += "up";
        if(keyListener.down())
            dirString += "down";
        if(keyListener.left())
            dirString += "left";
        if(keyListener.right())
            dirString += "right";
        super.movement(dirString,game);
        updateCamera(game.getRenderer().getCamera());
    }


    @Override
    public void render(RenderHandler renderer, int xZoom, int yZoom) {
        super.render(renderer, xZoom, yZoom);
    }

    public void updateCamera(Rectangle camera){
        camera.x = super.getCharacterRectangle().x - (camera.w/2);
        camera.y = super.getCharacterRectangle().y - (camera.h/2);
    }
    public int getLayer() {
        return 0;
    }

    @Override
    public Rectangle getRectangle() {
        return super.getRectangle();
    }
}
