import java.awt.image.BufferedImage;

public class Sprite {

    int width, height;
    int[] pixels;

    Sprite(SpriteSheet sheet, int startX, int startY, int width, int height){
      this.width = width;
      this.height = height;

      pixels = new int[width*height];
      sheet.getBufferedImage().getRGB(startX, startY, width, height, pixels, 0, width);
    }

    Sprite(BufferedImage image){
        width = image.getWidth();
        height = image.getHeight();

        pixels = new int[width * height];
        image.getRGB(0,0,width,height,pixels,0,width);
    }

    Sprite(){}

    public int getWidth(){
        return width;
    }
    public int getHeight() {
        return height;
    }
    public int[] getPixels() {
        return pixels;
    }
}
