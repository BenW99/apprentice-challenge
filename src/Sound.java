import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class Sound {

    private Clip clip;


    //Call this function and pass it the file path of the sound to play it
    public void playSound(String file) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        File f;
        AudioInputStream ais;

        f = new File(file);
        ais = AudioSystem.getAudioInputStream(f);
        clip = AudioSystem.getClip();
        clip.open(ais);
        clip.start();
    }

    public void loopSound(String file) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        File f;
        AudioInputStream ais;

        f = new File(file);
        ais = AudioSystem.getAudioInputStream(f);
        clip = AudioSystem.getClip();
        clip.open(ais);
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void stopSound() {
        clip.stop();
    }
}
