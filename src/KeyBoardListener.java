import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyBoardListener implements KeyListener, FocusListener{

    public boolean[] keys = new boolean[120];
    public int upKey = KeyEvent.VK_UP;
    public int downKey = KeyEvent.VK_DOWN;
    public int leftKey = KeyEvent.VK_LEFT;
    public int rightKey = KeyEvent.VK_RIGHT;
    public Game game;

    public void setGame(Game game){
        this.game = game;
    }
    @Override
    public void focusGained(FocusEvent e) {

    }

    @Override
    public void focusLost(FocusEvent e) {
        for(int i=0; i<keys.length;i++)
            keys[i] = false;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if(keyCode<keys.length)
            keys[keyCode] = true;
        if(keys[KeyEvent.VK_CONTROL]){
            game.handleCTRL(keys);
        }
        if(keys[KeyEvent.VK_ESCAPE]){
            game.ingameMenu();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if(keyCode<keys.length)
            keys[keyCode] = false;
    }

    public void setUp(int key) {
        upKey = key;
    }

    public void setDown(int key) {
        downKey = key;
    }

    public void setRight(int key) {
        rightKey = key;
    }

    public void setLeft(int key) {
        leftKey = key;
    }

    public void resetKeys(){
        upKey = KeyEvent.VK_UP;
        downKey = KeyEvent.VK_DOWN;
        leftKey = KeyEvent.VK_LEFT;
        rightKey = KeyEvent.VK_RIGHT;
    }

    public int getUpKey() {
        return upKey;
    }

    public int getDownKey() {
        return downKey;
    }

    public int getLeftKey() {
        return leftKey;
    }

    public int getRightKey() {
        return rightKey;
    }

    public boolean up(){ return keys[upKey]; }
    public boolean down(){ return keys[downKey]; }
    public boolean left() { return keys[leftKey]; }
    public boolean right(){ return keys[rightKey]; }
}
