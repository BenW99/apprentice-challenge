import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;


public class RenderHandler 
{
	private BufferedImage view;
	private Rectangle camera;
	private int[] pixels;
	private int maxWidth = 0, maxHeight = 0;

	public RenderHandler(int width, int height) 
	{
		GraphicsDevice[] graphicsDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();

		for(int i=0; i<graphicsDevices.length; i++){
			if(maxWidth < graphicsDevices[i].getDisplayMode().getWidth())
				maxWidth = graphicsDevices[i].getDisplayMode().getWidth();
			if(maxHeight<graphicsDevices[i].getDisplayMode().getHeight())
				maxHeight = graphicsDevices[i].getDisplayMode().getHeight();
		}

		//Create a BufferedImage that will represent our view.
		view = new BufferedImage(maxWidth, maxHeight, BufferedImage.TYPE_INT_RGB);

		camera = new Rectangle(0,0,width, height);
		camera.x = 0;
		camera.y = 0;

		//Create an array for pixels
		pixels = ((DataBufferInt) view.getRaster().getDataBuffer()).getData();
	}


	public void render(Graphics graphics)
	{
		graphics.drawImage(view.getSubimage(0,0,camera.w,camera.h), 0, 0, camera.w, camera.h, null);
	}

	public void renderArray(int[] renderPixels, int renderWidth, int renderHeight, int xOffset, int yOffset, int xZoom, int yZoom){
		for(int y=0; y<renderHeight; y++)
			for(int x=0; x<renderWidth; x++)
				for(int yZoomPosition = 0; yZoomPosition < yZoom; yZoomPosition++)
					for (int xZoomPosition = 0; xZoomPosition < xZoom; xZoomPosition++)
						setPixel(((x*xZoom) + xOffset + xZoomPosition), ((y*yZoom) + yOffset + yZoomPosition), renderPixels[x + y * renderWidth]);
	}

	public void renderSprite(Sprite sprite, int xOffset, int yOffset, int xZoom, int yZoom){
		renderArray(sprite.getPixels(), sprite.getWidth(), sprite.getHeight(), xOffset, yOffset, xZoom, yZoom);
	}
	public void renderRectangle(Rectangle rectangle, int xZoom, int yZoom){
		int[] rectanglePixels = rectangle.getPixels();
		if(rectanglePixels!=null){
			renderArray(rectanglePixels, rectangle.w, rectangle.h, rectangle.x, rectangle.y, xZoom, yZoom);
		}
	}
	public void renderImage(BufferedImage image, int xOffset, int yOffset, int xZoom, int yZoom){
		int[] imagePixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
		renderArray(imagePixels, image.getWidth(), image.getHeight(), xOffset, yOffset, xZoom, yZoom);
	}


	private void setPixel(int x, int y, int pixel) {
		if (x >= camera.x && y >= camera.y && x<= camera.x + camera.w && y<= camera.y + camera.h) {
			int pixelIndex = (x - camera.x) + (y-camera.y) * view.getWidth();
			if (pixels.length > pixelIndex && pixel!=Game.alpha)
				pixels[pixelIndex] = pixel;
		}
	}


	public Rectangle getCamera() {
		return camera;
	}

	public void clear(){
		for(int i=0; i<pixels.length; i++){
			pixels[i] = 0;
		}
	}

	public int getMaxHeight() {
		return maxHeight;
	}
	public int getMaxWidth(){
		return maxWidth;
	}
}