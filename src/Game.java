import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.Runnable;
import java.lang.Thread;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import javax.imageio.ImageIO;

public class Game extends JFrame implements Runnable {
    private Canvas canvas = new Canvas();
    private RenderHandler renderer;
    private SpriteSheet sheet;
    private SpriteSheet playerSheet;
    private BufferedImage myPicture;
    private BufferedImage intro;
    AnimatedSprite playerAnim;

    private int selectedLayer = 0;

    public static int alpha = 0xFFFF00DC; //colour for making complete transparency

    private final int GAMEWIDTH = 800;
    private final int GAMEHEIGHT = 600;
    static final int TILESIZE = 32;
    private final int XZOOM = 2;
    private final int YZOOM = 2;

    static private final String theme = "./resources/themes/doctor-dale-theme.wav";

    private int placeTile = 2;
    private int mapNum = 0;
    private final String mapBase = "resources/Map";
    private Tiles tiles;
    private Map map;
    private boolean adminMode = false;
    private boolean gameStarted = false;

    private GameObject[] objects;
    private KeyBoardListener keyListener;
    private Menu menu;
    private MouseEventListener mouseListener = new MouseEventListener(this);

    private Player player;

    private Sound sound;


    public Game(Menu menu, KeyBoardListener keyListener, Sound sound) //Sets all the defaults for the game
    {
        this.sound = sound;
        try {
            sound.loopSound(theme);
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
        //Setters
        this.keyListener = keyListener;
        this.menu = menu;
        keyListener.setGame(this);

        //Make our program shutdown when we exit out.
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set the position and size of our frame.
        setBounds(0, 0, GAMEWIDTH, GAMEHEIGHT);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);

        //Put our frame in the center of the screen.
        setLocationRelativeTo(null);

        //Adds the canvas to the JFrame
        add(canvas);

        //Make our frame visible.
        setVisible(true);

        //Create our object for buffer strategy.
        canvas.createBufferStrategy(3);

        renderer = new RenderHandler(getWidth(), getHeight());

        BufferedImage sheetImage = loadImage("cavernTiles1.png");
        myPicture = loadImage("doctor-dale-disclamer.png");
        intro = loadImage("into-on-floor.png");

        sheet = new SpriteSheet(sheetImage);
        sheet.loadSprites(TILESIZE, TILESIZE);

        BufferedImage playerSheetImage = loadImage("main_character.png");
        playerSheet = new SpriteSheet(playerSheetImage);
        playerSheet.loadSprites(64, 64);

        playerAnim = new AnimatedSprite(playerSheet, 5);

        objects = new GameObject[1];
        player = new Player(playerAnim, XZOOM, YZOOM);
        objects[0] = player;

		tiles = new Tiles(new File("resources/Tiles.txt"), sheet);
		map = new Map(new File(mapBase + mapNum + ".txt"), tiles);

        canvas.addKeyListener(keyListener);
        canvas.addFocusListener(keyListener);
        canvas.addMouseListener(mouseListener);
        canvas.addMouseMotionListener(mouseListener);

        //Sets the focus
        canvas.requestFocusInWindow();

        addComponentListener(new ComponentListener() {
            public void componentResized(ComponentEvent e) {
                int newWidth = canvas.getWidth();
                int newHeight = canvas.getHeight();

                if(newWidth > renderer.getMaxWidth())
                    newWidth = renderer.getMaxWidth();

                if(newHeight > renderer.getMaxHeight())
                    newHeight = renderer.getMaxHeight();

                renderer.getCamera().w = newWidth;
                renderer.getCamera().h = newHeight;
                canvas.setSize(newWidth, newHeight);
                pack();
            }

            public void componentHidden(ComponentEvent e) {}
            public void componentMoved(ComponentEvent e) {}
            public void componentShown(ComponentEvent e) {}
        });
        canvas.requestFocus();
    }
    private void changeMap() {
        mapNum++;
        File mapFile = new File(mapBase + mapNum + ".txt");
        if(mapFile.exists())
            map = new Map(mapFile, tiles);
    }

    public void ingameMenu(){
        Object[] options = {"OK", "CANCEL"};
        int result = JOptionPane.showOptionDialog(
                null,
                "Do you want to quit?",
                "Quit?",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[0]
                );
        if (result == JOptionPane.YES_OPTION){
            menu.setVisible(true);
            remove(canvas);
            dispose();
            sound.stopSound();
        }
    }

    public void handleCTRL(boolean[] keys) {
        if (keys[KeyEvent.VK_S]) {
            if (adminMode) map.saveMap();
        }
        if (keys[KeyEvent.VK_EQUALS]) {
            incrementPlaceTile();
        }
        if (keys[KeyEvent.VK_MINUS]) {
            decrementPlaceTile();
        }
        if (keys[KeyEvent.VK_L]) {
            toggleAdmin();
        }
        if(keys[KeyEvent.VK_Q]){
            changeMap();
        }
        if(keys[KeyEvent.VK_O]){
            selectedLayer--;
            System.out.println(selectedLayer);
        }
        if(keys[KeyEvent.VK_P]){
            selectedLayer++;
        }
    }

    private void incrementPlaceTile() {
        if (placeTile < Tiles.getTileListLen())
            placeTile++;
        else
            placeTile -= Tiles.getTileListLen();
    }

    private void decrementPlaceTile() {
        if (placeTile > 0)
            placeTile--;
        else
            placeTile += Tiles.getTileListLen();
    }

    private void toggleAdmin() {
        if (!adminMode) {
            adminMode = true;
            System.out.println("Dastardly Admin Limits Enabled");
        } else {
            adminMode = false;
            System.out.println("Dastardly Admin Limits Disabled");
        }
    }

    public void leftClick(int x, int y) {
        if (adminMode) {
            x = (int) Math.floor((x + renderer.getCamera().x) / ((double) TILESIZE * XZOOM));
            y = (int) Math.floor((y + renderer.getCamera().y) / ((double) TILESIZE * YZOOM));
            map.setTile(selectedLayer, x, y, placeTile);
            System.out.println(placeTile);
        }
    }

    public void rightClick(int x, int y) {
        if (adminMode) {
            x = (int) Math.floor((x + renderer.getCamera().x) / ((double) TILESIZE * XZOOM));
            y = (int) Math.floor((y + renderer.getCamera().y) / ((double) TILESIZE * YZOOM));
            map.unsetTile(selectedLayer, x, y);
        }
    }

    public void update() { //update all variables
        for (int i = 0; i < objects.length; i++)
            objects[i].update(this);
    }

    private BufferedImage loadImage(String path) {
        try {
            BufferedImage loadedImage = ImageIO.read(Game.class.getResource(path));
            BufferedImage formattedImage = new BufferedImage(loadedImage.getWidth(), loadedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
            formattedImage.getGraphics().drawImage(loadedImage, 0, 0, null);
            return formattedImage;
        } catch (IOException imageException) {
            imageException.printStackTrace();
            return null;
        }
    }

    public void render() {
        BufferStrategy bufferStrategy = canvas.getBufferStrategy();
        Graphics graphics = bufferStrategy.getDrawGraphics();
        super.paint(graphics);


//
        map.render(renderer, objects, XZOOM, YZOOM);

        if(mapNum==0){
            renderer.renderImage(myPicture,-335,24*TILESIZE,1,1);
            renderer.renderImage(intro, -334, 1700, 1, 1);
            renderer.renderSprite(playerAnim,objects[0].getRectangle().x, objects[0].getRectangle().y, XZOOM, YZOOM);
        }

        if (adminMode) map.renderAdminTile(placeTile, renderer, XZOOM, YZOOM);

//        renderer.renderRectangle(player.getRectangle(), 1, 1);
//        renderer.renderRectangle(player.getCollRectangle(), 1, 1);


        renderer.render(graphics);

        graphics.dispose();
        bufferStrategy.show();
    }//renders all pixels to the screen

    public void run() {
        BufferStrategy bufferStrategy = canvas.getBufferStrategy();
        int i = 0;
        int x = 0;

        long lastTime = System.nanoTime(); //long 2^63
        double nanoSecondConversion = 1000000000.0 / 60; //60 frames per second
        double changeInSeconds = 0;

        while (true) {
            long now = System.nanoTime();

            changeInSeconds += (now - lastTime) / nanoSecondConversion;
            while (changeInSeconds >= 1) {
                update();
                changeInSeconds--;
            }

            render();
            lastTime = now;
        }
    }

    public KeyBoardListener getKeyListener() {
        return keyListener;
    }

    public RenderHandler getRenderer() {
        return renderer;
    }

    public Map getMap() {
        return map;
    }

    public int getXZoom() {
        return XZOOM;
    }

    public int getYZoom() {
        return YZOOM;
    }

}
