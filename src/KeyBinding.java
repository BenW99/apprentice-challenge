import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.KeyEvent;

public class KeyBinding extends JPanel {

    private JTextField textFieldUp = new JTextField(1);
    private JTextField textFieldDown = new JTextField(1);
    private JTextField textFieldLeft = new JTextField(1);
    private JTextField textFieldRight = new JTextField(1);
    private JLabel labelUp = new JLabel("Current value is the UP key");
    private JLabel labelDown = new JLabel("Current value is the DOWN key");
    private JLabel labelLeft = new JLabel("Current value is the LEFT key");
    private JLabel labelRight = new JLabel("Current value is the RIGHT key");

    public KeyBinding(JPanel cards, CardLayout cl, KeyBoardListener keyListener){
        labelUp.setBackground(Color.decode("#D3D3D3"));
        labelUp.setOpaque(true);
        labelDown.setBackground(Color.decode("#D3D3D3"));
        labelDown.setOpaque(true);
        labelLeft.setBackground(Color.decode("#D3D3D3"));
        labelLeft.setOpaque(true);
        labelRight.setBackground(Color.decode("#D3D3D3"));
        labelRight.setOpaque(true);

        setLayout(new GridLayout(1,2));
        setSize(800, 600);
        setLayout(new GridLayout(5,2, 80,30));
        add(createLabel("Up"));
        add(textFieldUp);
        if (keyListener.getUpKey() != 38){
            labelUp.setText("Current value is " + (char) keyListener.getUpKey());
        }
        add(labelUp);
        add(createLabel("Left"));
        add(textFieldLeft);
        if (keyListener.getLeftKey() != 37){
            labelLeft.setText("Current value is " + (char) keyListener.getLeftKey());
        }
        add(labelLeft);
        add(createLabel("Down"));
        add(textFieldDown);
        if (keyListener.getDownKey() != 40){
            labelDown.setText("Current value is " + (char) keyListener.getDownKey());
        }
        add(labelDown);
        add(createLabel("Right"));
        add(textFieldRight);
        if (keyListener.getRightKey() != 39){
            labelRight.setText("Current value is " + (char) keyListener.getRightKey());
        };
        add(labelRight);
        add(createButton("Save", cards, cl, keyListener));
        add(createButton("Reset to arrow keys", cards, cl, keyListener));
        add(createButton("Back", cards, cl, keyListener));
    }

    private void updateLabel(KeyBoardListener keyListener){
        if (keyListener.getUpKey() == 38){
            labelUp.setText("Current value is the UP key");
        }else{
            labelUp.setText("Current value is " + (char) keyListener.getUpKey());
        }

        if (keyListener.getLeftKey() == 37){
            labelLeft.setText("Current value is the LEFT key");
        }else{
            labelLeft.setText("Current value is " + (char) keyListener.getLeftKey());
        }

        if (keyListener.getDownKey() == 40){
            labelDown.setText("Current value is the DOWN key");
        }else{
            labelDown.setText("Current value is " + (char) keyListener.getDownKey());
        }

        if (keyListener.getRightKey() == 39){
            labelRight.setText("Current value is the RIGHT key");
        }else{
            labelRight.setText("Current value is " + (char) keyListener.getRightKey());
        }
    }

    private JButton createButton(String title, JPanel cards, CardLayout cl, KeyBoardListener keyListener)
    {
        JButton button = new JButton(title);
        button.setPreferredSize(new Dimension(200, 200));
        button.setBackground(Color.decode("#d3d3d3"));
        class buttonChange implements ActionListener {
            public void actionPerformed(ActionEvent event) {
                if (title.equals("Save")) {
                    keyListener.setUp(KeyEvent.getExtendedKeyCodeForChar(textFieldUp.getText().charAt(0)));
                    keyListener.setDown(KeyEvent.getExtendedKeyCodeForChar(textFieldDown.getText().charAt(0)));
                    keyListener.setLeft(KeyEvent.getExtendedKeyCodeForChar(textFieldLeft.getText().charAt(0)));
                    keyListener.setRight(KeyEvent.getExtendedKeyCodeForChar(textFieldRight.getText().charAt(0)));
                    updateLabel(keyListener);
                } else if (title.equals("Reset to arrow keys")){
                    keyListener.resetKeys();
                    updateLabel(keyListener);
                } else if (title.equals("Back")) {
                    cl.show(cards, "options");
                }
            }
        }
        ActionListener listener = new buttonChange();
        button.addActionListener(listener);
        return button;
    }

    private JLabel createLabel(String title){
        JLabel label = new JLabel(title, SwingConstants.CENTER);
        label.setBackground(Color.decode("#D3D3D3"));
        label.setOpaque(true);
        return label;
    }
}
