public class MappedTile {
    public int layer,type,x,y;

    MappedTile(int layer, int type, int x, int y){
        this.layer = layer;
        this.type = type;
        this.x = x;
        this.y = y;
    }
}
