import java.awt.image.BufferedImage;

public class AnimatedSprite extends Sprite implements GameObject {

    private Sprite[] sprites;
    private int currentSprite = 0;
    private int speed;
    private int framesPassed = 0;

    private int startSprite;
    private int endSprite;

    AnimatedSprite(SpriteSheet sheet, Rectangle[] positions, int speed){
        sprites = new Sprite[positions.length];
        this.startSprite = 0;
        this.endSprite = positions.length-1;
        this.speed = speed;
        for (int i=0; i<positions.length; i++){
            sprites[i] = new Sprite(sheet, positions[i].x, positions[i].y, positions[i].w, positions[i].h);
        }

    }
    AnimatedSprite(SpriteSheet sheet, int speed){
        sprites = sheet.getLoadedSprites();
        this.speed = speed;
        this.endSprite = sprites.length-1;
    }
    AnimatedSprite(BufferedImage[] image, int framesToChange) {
        sprites = new Sprite[image.length];
        this.speed = framesToChange;
        for(int i=0; i<image.length; i++){
            sprites[i] = new Sprite(image[i]);
        }
    }

    @Override
    public void render(RenderHandler renderer, int xZoom, int yZoom) {}

    @Override
    public void update(Game game) {
        framesPassed++;
        if(framesPassed>=speed){
            framesPassed = 0;
            incrementSprite();
        }
    }

    public void incrementSprite(){
        currentSprite++;
        if(currentSprite>=endSprite){
            currentSprite=startSprite;
        }
    }

    public void setAnimationRange(int startSprite, int endSprite){
        this.startSprite = startSprite;
        this.endSprite = endSprite;
        reset();
    }

    public void reset(){
        framesPassed = 0;
        currentSprite = startSprite;
    }

    @Override
    public int getWidth(){return sprites[currentSprite].getWidth();}

    @Override
    public int getHeight() {return sprites[currentSprite].getHeight();}

    @Override
    public int[] getPixels(){return sprites[currentSprite].getPixels();}

    public int getLayer() {
        System.out.println("Called getLayer() of AnimatedSprite! This has no meaning here.");
        return -1;
    }
    public Rectangle getRectangle() {
        System.out.println("Called getRectangle() of AnimatedSprite! This has no meaning here.");
        return null;
    }
}