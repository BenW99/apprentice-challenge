
public abstract class Character implements GameObject{

    private Rectangle characterRectangle;
    private Sprite sprite;
    private AnimatedSprite animatedSprite = null;
    private int dir = 0;
    private int speed;
    private int healthPoints;
    private int attackDamage;
    private Rectangle collisionCheckRectangle;
    private int layer = 0;
    private final int xCollisionOffset = 40;
    private final int yCollisionOffset = 85;

    public int getSpeed() {
        return speed;
    }
    public void setSpeed(int speed){
        this.speed = speed;
    }

    public Rectangle getCharacterRectangle() {
        return characterRectangle;
    }

    public int getAttackDamage(){
        return attackDamage;
    }

    public void setAttackDamage(int attackDamage){
        this.attackDamage = attackDamage;
    }

    public int getHealthPoints(){
        return healthPoints;
    }

    public void setHealthPoints(int healthPoints){
        this.healthPoints = healthPoints;
    }

    Character(Sprite sprite, int speed, int baseHp, int baseAttackDamage, int xZoom, int yZoom){
        this.speed = speed;
        this.healthPoints = baseHp;
        this.attackDamage = baseAttackDamage;
        this.sprite = sprite;
        if(sprite instanceof AnimatedSprite)
            animatedSprite = (AnimatedSprite)sprite;
        characterRectangle = new Rectangle(0, 0, 64, 64);
        characterRectangle.generateGraphics(3, 0xFF00FF90);
        collisionCheckRectangle = new Rectangle(0, 0, 20*xZoom, 15*yZoom);
    }

    public void movement(String direction, Game game){
        int newDir = dir;
        boolean moved = false;

        collisionCheckRectangle.x = characterRectangle.x;
        collisionCheckRectangle.y = characterRectangle.y;

        if (direction.contains("up")){
            collisionCheckRectangle.y -= speed;
            newDir = 0;
            moved = true;
        }

        if (direction.contains("down")){
            collisionCheckRectangle.y += speed;
            newDir = 2;
            moved = true;
        }

        if (direction.contains("left")){
            collisionCheckRectangle.x -= speed;
            newDir = 1;
            moved = true;
        }

        if (direction.contains("right")){
            collisionCheckRectangle.x += speed;
            newDir = 3;
            moved = true;
        }

        if(moved) {
            collisionCheckRectangle.x += xCollisionOffset;
            collisionCheckRectangle.y += yCollisionOffset;

            Rectangle axisCheck = new Rectangle(collisionCheckRectangle.x, characterRectangle.y + yCollisionOffset, collisionCheckRectangle.w, collisionCheckRectangle.h);

            //Check the X axis
            if(!game.getMap().checkCollision(axisCheck, layer, game.getXZoom(), game.getYZoom()) &&
                    !game.getMap().checkCollision(axisCheck, layer + 1, game.getXZoom(), game.getYZoom())) {
                characterRectangle.x = collisionCheckRectangle.x - xCollisionOffset;
            }

            axisCheck.x = characterRectangle.x + xCollisionOffset;
            axisCheck.y = collisionCheckRectangle.y;
            axisCheck.w = collisionCheckRectangle.w;
            axisCheck.h = collisionCheckRectangle.h;
            //axisCheck = new Rectangle(playerRectangle.x, collisionCheckRectangle.y, collisionCheckRectangle.w, collisionCheckRectangle.h);

            //Check the Y axis
            if(!game.getMap().checkCollision(axisCheck, layer, game.getXZoom(), game.getYZoom()) &&
                    !game.getMap().checkCollision(axisCheck, layer + 1, game.getXZoom(), game.getYZoom())) {
                characterRectangle.y = collisionCheckRectangle.y - yCollisionOffset;
            }
            if (animatedSprite != null)
                animatedSprite.update(game);
        }else {
            animatedSprite.reset();
        }
        if(newDir!=dir){
            dir = newDir;
            updateDirection();
        }
    }

    private void updateDirection(){
        if(animatedSprite!=null){
            animatedSprite.setAnimationRange(dir*9, dir*9+9);
        }
    }

    public void render(RenderHandler renderer, int xZoom, int yZoom){
        if(animatedSprite!=null)
            renderer.renderSprite(animatedSprite,characterRectangle.x, characterRectangle.y, xZoom, yZoom);
        else if(sprite!= null)
            renderer.renderSprite(sprite, characterRectangle.x, characterRectangle.y, xZoom, yZoom);
        else
            renderer.renderRectangle(characterRectangle, xZoom, yZoom);
    }
    public abstract void update(Game game);

    public void attack(){}

    public void healthRegen(){}

    public void modifyHP(int modifier) {
        healthPoints += modifier;
    }

    public void modifyHP(float multiplier) {
        if(multiplier >= 0) { // allows the modifier 0 so we can have instadeath - traps etc.
            healthPoints = (int) (healthPoints * multiplier);
        }
    }
    public int getLayer() {
        return layer;
    }
    public Rectangle getRectangle() {
        return characterRectangle;
    }

    public Rectangle getCollRectangle() {
        collisionCheckRectangle.generateGraphics(3,6372);
        return collisionCheckRectangle;
    }

}
