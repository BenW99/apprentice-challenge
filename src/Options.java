import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Options extends JPanel {

    public Options(JPanel cards, CardLayout cl, Boolean sound)
    {
        setLayout(new GridBagLayout());
        setSize(800, 600);
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1,3, 80,0));
        panel.add(createButton("Key Bindings", cards, cl, sound));
        panel.add(createButton("Back", cards, cl, sound));
        panel.setOpaque(false);
        add(panel, new GridBagConstraints());
    }

    private JButton createButton(String title, JPanel cards, CardLayout cl, Boolean sound)
    {
        JButton button = new JButton(title);
        button.setPreferredSize(new Dimension(150, 100));
        button.setBackground(Color.decode("#d3d3d3"));
        class buttonChange implements ActionListener
        {
            public void actionPerformed(ActionEvent event)
            {
                if (title.equals("Key Bindings")){
                    cl.show(cards, "keyBinding");
                }else if(title.equals("Back")){
                    cl.show(cards, "menu");
                }
            }
        }
        ActionListener listener = new buttonChange();
        button.addActionListener(listener);
        return button;
    }
}

