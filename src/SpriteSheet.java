import java.awt.image.BufferedImage;

public class SpriteSheet {
    private int[] pixels;
    private BufferedImage bufferedImage;
    public final int WIDTH;
    public final int HEIGHT;
    private Sprite[] loadedSprites = null;
    private boolean spritesLoaded = false;

    private int spriteSizeX;

    SpriteSheet(BufferedImage sheetImage){
        WIDTH = sheetImage.getWidth();
        HEIGHT = sheetImage.getHeight();
        bufferedImage = sheetImage;

        pixels = new int[WIDTH*HEIGHT];
        pixels = sheetImage.getRGB(0,0, WIDTH, HEIGHT, pixels, 0, WIDTH);
    }
    public void loadSprites(int spriteSizeX, int spriteSizeY){
        this.spriteSizeX = spriteSizeX;
        loadedSprites = new Sprite[(WIDTH / spriteSizeX)*(HEIGHT/spriteSizeY)];
        int spriteID = 0;
        for(int y=0; y<HEIGHT;y+=spriteSizeY){
            for(int x=0; x<WIDTH; x+=spriteSizeX){
                loadedSprites[spriteID] = new Sprite(this, x, y, spriteSizeX,spriteSizeY);
                spriteID++;
            }
        }
        spritesLoaded = true;
    }

    public Sprite getSprite(int x, int y){
        if(spritesLoaded){
            int spriteID = x + y*(WIDTH / spriteSizeX);
            if(spriteID<loadedSprites.length)
                return loadedSprites[spriteID];
            else
                System.out.println("SpriteID out of bounds");
        }else{
            System.out.println("Sprites not loaded from spritesheet");
        }
        return null;
    }
    public int[] getPixels(){
        return pixels;
    }
    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public Sprite[] getLoadedSprites(){
        return loadedSprites;
    }
}
